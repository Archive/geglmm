// -*- c++ -*-
#ifndef _LIBGEGLMM_INIT_H
#define _LIBGEGLMM_INIT_H
/* libgeglmm - a C++ wrapper for libgegl
 *
 * (c) 2008 Hubert Figuiere
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <http://www.gnu.org/licenses/>.
 */

#include <glibmm.h>


namespace Gegl
{

void init(int *nargs, gchar **args[]);
void exit();

} //namespace Gegl

#endif //_LIBGEGLMM_INIT_H

