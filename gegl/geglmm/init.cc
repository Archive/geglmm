// -*- c++ -*-
/* libgeglmm - a C++ wrapper for libgegl
 *
 * (c) 2008 Hubert Figuiere
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <http://www.gnu.org/licenses/>.
 */

#include <geglmm/init.h>
#include <glibmm/init.h>
#include <geglmmconfig.h> //For LIBGEGLMM_VERSION
#include <geglmm/wrap_init.h>
#include <gegl.h>


namespace Gegl
{

void init(int *nargs, gchar **args[])
{
  Glib::init(); //Sets up the g type system and the Glib::wrap() table.
  wrap_init(); //Tells the Glib::wrap() table about the geglmm classes.
  gegl_init(nargs, args);
}


void exit()
{
  gegl_exit();
}

} //namespace Gegl
