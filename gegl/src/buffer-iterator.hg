/* libgeglmm - a C++ wrapper for libgegl
 *
 * (c) 2011 Hubert Figuiere
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <http://www.gnu.org/licenses/>.
 */

#include <gegl.h>
#include <gegl-utils.h>

#include <geglmm/buffer.h>

_DEFS(geglmm,gegl)

namespace Gegl {

class BufferIterator
{
  _CLASS_GENERIC(BufferIterator, GeglBufferIterator)
public:
  BufferIterator(GeglBufferIterator* p) 
  {
  }

  GeglBufferIterator*       gobj()       { return reinterpret_cast<GeglBufferIterator*>(this); }
  const GeglBufferIterator* gobj() const { return reinterpret_cast<const GeglBufferIterator*>(this); }

  int add(const Glib::RefPtr<Buffer>& buffer, const Rectangle& roi, 
          int level, const Babl* format, unsigned int flags,
	  AbyssPolicy abyss_policy);

  bool next();

protected:
  GeglBufferIterator gobject_;
private:
  BufferIterator(const BufferIterator&);
  BufferIterator& operator=(const BufferIterator&);
};

}

namespace Glib {

Gegl::BufferIterator* wrap(GeglBufferIterator* object);

}
