/* libgeglmm - a C++ wrapper for libgegl
 *
 * (c) 2011 Hubert Figuiere
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <http://www.gnu.org/licenses/>.
 */



#include <gegl.h>

namespace Gegl {

int BufferIterator::add(const Glib::RefPtr<Gegl::Buffer>& buffer, const Gegl::Rectangle& roi, int level,
          const Babl* format, unsigned int flags, AbyssPolicy abyss_policy)
{
  return gegl_buffer_iterator_add(gobj(), buffer->gobj(), roi.gobj(), level, format, flags, (GeglAbyssPolicy)abyss_policy);
}

bool BufferIterator::next()
{
  return gegl_buffer_iterator_next(gobj());
}


}

namespace Glib
{

Gegl::BufferIterator *wrap(GeglBufferIterator* object)
{
  return new(object) Gegl::BufferIterator(object);
}

} /* namespace Glib */
