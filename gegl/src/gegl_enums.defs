;; From gegl-enums.h

;; Original typedef:
;; typedef enum {
;;   GEGL_SAMPLER_NEAREST = 0,   /*< desc="nearest"      >*/
;;   GEGL_SAMPLER_LINEAR,        /*< desc="linear"       >*/
;;   GEGL_SAMPLER_CUBIC,         /*< desc="cubic"        >*/
;;   GEGL_SAMPLER_LOHALO         /*< desc="lohalo"       >*/
;; } GeglSamplerType;

(define-enum-extended SamplerType
  (in-module "Gegl")
  (c-name "GeglSamplerType")
  (values
    '("nearest" "GEGL_SAMPLER_NEAREST" "0")
    '("linear" "GEGL_SAMPLER_LINEAR" "1")
    '("cubic" "GEGL_SAMPLER_CUBIC" "2")
    '("lohalo" "GEGL_SAMPLER_LOHALO" "3")
  )
)

;; Original typedef:
;; typedef enum {
;;   GEGL_ABYSS_NONE
;; } GeglAbyssPolicy;

(define-enum-extended AbyssPolicy
  (in-module "Gegl")
  (c-name "GeglAbyssPolicy")
  (values
    '("none" "GEGL_ABYSS_NONE" "0")
  )
)

;; Original typedef:
;; typedef enum {
;;   GEGl_RIPPLE_WAVE_TYPE_SINE,
;;   GEGl_RIPPLE_WAVE_TYPE_SAWTOOTH
;; } GeglRippleWaveType;

(define-enum-extended RippleWaveType
  (in-module "Gegl")
  (c-name "GeglRippleWaveType")
  (values
    '("sine" "GEGl_RIPPLE_WAVE_TYPE_SINE" "0")
    '("sawtooth" "GEGl_RIPPLE_WAVE_TYPE_SAWTOOTH" "1")
  )
)

;; Original typedef:
;; typedef enum
;; {
;;   GEGL_WARP_BEHAVIOR_MOVE,      /*< desc="Move pixels"              >*/
;;   GEGL_WARP_BEHAVIOR_GROW,      /*< desc="Grow area"                >*/
;;   GEGL_WARP_BEHAVIOR_SHRINK,    /*< desc="Shrink area"              >*/
;;   GEGL_WARP_BEHAVIOR_SWIRL_CW,  /*< desc="Swirl clockwise"          >*/
;;   GEGL_WARP_BEHAVIOR_SWIRL_CCW, /*< desc="Swirl counter-clockwise"  >*/
;;   GEGL_WARP_BEHAVIOR_ERASE,     /*< desc="Erase warping"            >*/
;;   GEGL_WARP_BEHAVIOR_SMOOTH     /*< desc="Smooth warping"           >*/
;; } GeglWarpBehavior;

(define-enum-extended WarpBehavior
  (in-module "Gegl")
  (c-name "GeglWarpBehavior")
  (values
    '("move" "GEGL_WARP_BEHAVIOR_MOVE" "0")
    '("grow" "GEGL_WARP_BEHAVIOR_GROW" "1")
    '("shrink" "GEGL_WARP_BEHAVIOR_SHRINK" "2")
    '("swirl-cw" "GEGL_WARP_BEHAVIOR_SWIRL_CW" "3")
    '("swirl-ccw" "GEGL_WARP_BEHAVIOR_SWIRL_CCW" "4")
    '("erase" "GEGL_WARP_BEHAVIOR_ERASE" "5")
    '("smooth" "GEGL_WARP_BEHAVIOR_SMOOTH" "6")
  )
)

;; From gegl-types.h

;; Original typedef:
;; typedef enum
;; {
;;   GEGL_PARAM_PAD_OUTPUT = 1 << G_PARAM_USER_SHIFT,
;;   GEGL_PARAM_PAD_INPUT  = 1 << (G_PARAM_USER_SHIFT + 1)
;; } GeglPadType;

(define-flags-extended PadType
  (in-module "Gegl")
  (c-name "GeglPadType")
  (values
    '("am-pad-output" "GEGL_PARAM_PAD_OUTPUT" "1 << G_PARAM_USER_SHIFT")
  )
)

;; Original typedef:
;; typedef enum
;; {
;;   GEGL_BLIT_DEFAULT  = 0,
;;   GEGL_BLIT_CACHE    = 1 << 0,
;;   GEGL_BLIT_DIRTY    = 1 << 1
;; } GeglBlitFlags;

(define-flags-extended BlitFlags
  (in-module "Gegl")
  (c-name "GeglBlitFlags")
  (values
    '("default" "GEGL_BLIT_DEFAULT" "0x0")
    '("cache" "GEGL_BLIT_CACHE" "1 << 0")
    '("dirty" "GEGL_BLIT_DIRTY" "1 << 1")
  )
)

;; From gegl-cl-color.h

;; Original typedef:
;; typedef enum
;; {
;;   GEGL_CL_COLOR_NOT_SUPPORTED = 0,
;;   GEGL_CL_COLOR_EQUAL         = 1,
;;   GEGL_CL_COLOR_CONVERT       = 2
;; } gegl_cl_color_op;

(define-enum-extended gegl_cl_color_op
  (in-module "Gegl")
  (c-name "gegl_cl_color_op")
  (values
    '("not-supported" "GEGL_CL_COLOR_NOT_SUPPORTED" "0")
    '("equal" "GEGL_CL_COLOR_EQUAL" "1")
    '("convert" "GEGL_CL_COLOR_CONVERT" "2")
  )
)

