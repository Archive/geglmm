#include <geglmm/node.h>
#include <geglmm/init.h>
#include <geglmm/buffer.h>

gint
main (gint    argc,
      gchar **argv)
{
  Gegl::init(&argc, &argv);  /* initialize the GEGL library */
  Glib::RefPtr<Gegl::Buffer> buffer; /* instantiate a graph */
  Glib::RefPtr<Gegl::Node>   gegl;   /* the gegl graph we're using as a node factor */
  Glib::RefPtr<Gegl::Node>   write_buffer,
             shift,
             load;
  gchar *in_file;
  gchar *buf_file;
  gdouble x;
  gdouble y;



  if (argv[1]==NULL ||
      argv[2]==NULL ||
      argv[3]==NULL ||
      argv[4]==NULL)
    {
      g_print ("\nUsage: %s <gegl buffer> <image file> <x> <y>\n"
               "\nWrites an image into the GeglBuffer at the specified coordinates\n",
               argv[0]);
      return -1;
    }
  buf_file = argv[1];
  in_file = argv[2];
  x = atof (argv[3]);
  y = atof (argv[4]);

  buffer = Gegl::Buffer::create_from_file (buf_file);
  gegl = Gegl::Node::create();

  write_buffer = gegl->new_child ("operation", "gegl:write-buffer");
  write_buffer->set("buffer", buffer);
  shift      = gegl->new_child ("operation", "gegl:shift");
  shift->set ( "x", x).set("y", y);
  load        = gegl->new_child ("operation", "gegl:load");
  load->set ("path", Glib::ustring(in_file));

  load->link(shift)->link(write_buffer);
  write_buffer->process ();

  /* free resources used by the graph and the nodes it owns */
  // nothing to do, this is C++ :-)

  /* free resources globally used by GEGL */
  Gegl::exit ();

  return 0;
}
