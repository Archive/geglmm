#include <glib.h>
#include <geglmm/buffer.h>
#include <geglmm/init.h>
#include <geglmm/node.h>

gint
main (gint    argc,
      gchar **argv)
{
  Glib::RefPtr<Gegl::Buffer> buffer;
  Glib::RefPtr<Gegl::Node>   gegl, load_file, save_file;

  Gegl::init (&argc, &argv);


  if (argv[1]==NULL ||
      argv[2]==NULL)
    {
      g_print ("\nusage: %s in.png out.gegl\n\nCreates a GeglBuffer from an image file.\n\n", argv[0]);
      exit (-1);
    }

  gegl = Gegl::Node::create();
  load_file = gegl->new_child ( "operation", "gegl:load");
  load_file->set("path", Glib::ustring(argv[1]));
  save_file = gegl->new_child ( "operation", "gegl:save-buffer");
  save_file->set("buffer", buffer);

  load_file->link (save_file);
  save_file->process ();

  buffer->save (argv[2]);

  Gegl::exit ();
  return 0;
}
