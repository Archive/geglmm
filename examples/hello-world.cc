#include <cstdio>
#include <glib.h>
#include <geglmm/init.h>
#include <geglmm/node.h>
#include <geglmm/color.h>

gint
main (gint    argc,
      gchar **argv)
{
  Gegl::init (&argc, &argv);  /* initialize the GEGL library */

  {
    /* instantiate a graph */
    Glib::RefPtr<Gegl::Node> gegl(Gegl::Node::create ());

/*
This is the graph we're going to construct:
 
.-----------.
| display   |
`-----------'
   |
.-------.
| layer |
`-------'
   |   \
   |    \
   |     \
   |      |
   |   .------.
   |   | text |
   |   `------'
.------------------.
| fractal-explorer |
`------------------'

*/

    /*< The image nodes representing operations we want to perform */
    Glib::RefPtr<Gegl::Node> display = gegl->create_child("gegl:display");
    Glib::RefPtr<Gegl::Node> layer = gegl->new_child("operation", "gegl:layer");
    layer->set ("x", 2.0).set ("y", 4.0);
    Glib::RefPtr<Gegl::Node> text = gegl->new_child("operation", "gegl:text");
    text->set ("size", 10.0)
		.set ("color", Glib::RefPtr<Gegl::Color>(Gegl::Color::create ("rgb(1.0,1.0,1.0)")));
    Glib::RefPtr<Gegl::Node> mandelbrot = gegl->new_child ("operation", "gegl:fractal-explorer");
    mandelbrot->set ("width", 512).set ("height", 384);

    mandelbrot->link(layer)->link(display);
    text->connect_to ("output",  layer, "aux");

    /* request that the save node is processed, all dependencies will
     * be processed as well
     */
    {
      gint frame;
      gint frames = 200;

      for (frame=0; frame<frames; frame++)
        {
          gchar string[512];
          double t = frame * 1.0/frames;
          double cx = -1.76;
          double cy = 0.0;

#define INTERPOLATE(min,max) ((max)*(t)+(min)*(1.0-t))

          double xmin = INTERPOLATE(  cx-0.02, cx-2.5);
          double ymin = INTERPOLATE(  cy-0.02, cy-2.5);
          double xmax = INTERPOLATE(  cx+-1.02, cx+2.5);
          double ymax = INTERPOLATE(  cy+0.02, cy+2.5);

          if (xmin<-3.0)
            xmin=-3.0;
          if (ymin<-3.0)
            ymin=-3.0;


          mandelbrot->set ("xmin", xmin).set ("ymin", ymin).set ("xmax", xmax).set ("ymax", ymax);
          snprintf (string, sizeof string, "%1.3f,%1.3f %1.3f×%1.3f", xmin, ymin, xmax-xmin, ymax-ymin);
          text->set ("string", Glib::ustring(string));
          display->process ();
        }
    }

    /* free resources used by the graph and the nodes it owns */
  }

  /* free resources globally used by GEGL */
  Gegl::exit ();

  return 0;
}

