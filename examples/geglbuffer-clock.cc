#include <geglmm/node.h>
#include <geglmm/init.h>
#include <geglmm/color.h>

#include <sys/time.h>
#include <time.h>

gint
main (gint    argc,
      gchar **argv)
{
  if (argv[1]==NULL)
    {
      g_print ("\nUsage: %s <GeglBuffer>\n"
               "\n"
               "Continously writes a timestamp to 0,0 in the buffer\n", argv[0]);
      exit (-1);
    }

  Gegl::init (&argc, &argv);  /* initialize the GEGL library */

  Glib::RefPtr<Gegl::Node>   gegl;   /* the gegl graph we're using as a node factor */
  Glib::RefPtr<Gegl::Node>   display,
             text,
             layer,
             crop,
             shift,
             blank;

  gegl = Gegl::Node::create ();


  blank      = gegl->new_child ("operation", "gegl:color");
  blank->set ("value", Gegl::Color::create ("rgba(0.0,0.0,0.0,0.4)"));

  crop       = gegl->new_child ("operation", "gegl:crop");
  crop->set ("x", 0.0);
  crop->set ("y", 0.0);
  crop->set ("width", 260.0);
  crop->set ("height", 22.0);

  layer      = gegl->new_child ("operation", "gegl:layer");

  shift      = gegl->new_child ("operation", "gegl:shift");
  shift->set("x", 0.0);
  shift->set("y", 0.0);

  text       = gegl->new_child ("operation", "gegl:text");
  text->set ("size", 20.0);
                             /*      "color", gegl_color_new ("rgb(0.0,0.0,0.0)"),*/
  display    = gegl->new_child ("operation", "gegl:composite-buffer");
  display->set ("path", Glib::ustring(argv[1]));

  blank->link(crop)->link(layer)->link(shift)->link(display);
  text->connect_to("output", layer, "aux");

  /* request that the save node is processed, all dependencies will
   * be processed as well
   */
  {
    gint frame;
    gint frames = 1024;

    for (frame=0; frame<frames; frame++)
      {
        struct timeval tv;

        int t = gettimeofday(&tv, NULL);
		text->set("string", Glib::ustring(ctime((const time_t*)&t)));
		display->process();
        g_usleep (1000000);
      }
  }
  /* free resources globally used by GEGL */
  Gegl::exit ();

  return 0;
}
