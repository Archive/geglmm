_EQUAL(glong,long)


_CONV_ENUM(Gegl,BlitFlags)
_CONV_ENUM(Gegl,SamplerType)
_CONV_ENUM(Gegl,AbyssPolicy)


#_CONVERSION(`GeglNode*',`const Glib::RefPtr<Node>&',Glib::wrap($3))
_CONVERSION(`GeglNode*',`Glib::RefPtr<Node>',Glib::wrap($3))
_CONVERSION(`GeglNode*',`Glib::RefPtr<const Node>',Glib::wrap($3))
_CONVERSION(`const Glib::RefPtr<Node>&',`GeglNode*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`const Glib::RefPtr<Node>',`GeglNode*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`Glib::RefPtr<Node>',`GeglNode*',__CONVERT_REFPTR_TO_P)

#_CONVERSION(`GeglProcessor*',`const Glib::RefPtr<Processor>&',Glib::wrap($3))
_CONVERSION(`GeglProcessor*',`Glib::RefPtr<Processor>',Glib::wrap($3))
_CONVERSION(`GeglProcessor*',`Glib::RefPtr<const Processor>',Glib::wrap($3))
_CONVERSION(`const Glib::RefPtr<Processor>&',`GeglProcessor*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`const Glib::RefPtr<Processor>',`GeglProcessor*',__CONVERT_REFPTR_TO_P)

_CONVERSION(`GeglBuffer*',`Glib::RefPtr<Buffer>',Glib::wrap($3))
_CONVERSION(`GeglBuffer*',`Glib::RefPtr<const Buffer>',Glib::wrap($3))
_CONVERSION(`const Glib::RefPtr<Buffer>&',`GeglBuffer*',__CONVERT_REFPTR_TO_P)

_CONVERSION(`GeglCurve*',`Glib::RefPtr<Curve>',Glib::wrap($3))
_CONVERSION(`GeglCurve*',`Glib::RefPtr<const Curve>',Glib::wrap($3))
_CONVERSION(`const Glib::RefPtr<Curve>&',`GeglCurve*',__CONVERT_REFPTR_TO_P)

_CONVERSION(`GeglOperation*',`Glib::RefPtr<Operation>',Glib::wrap($3))
_CONVERSION(`GeglOperation*',`Glib::RefPtr<const Operation>',Glib::wrap($3))
_CONVERSION(`const Glib::RefPtr<Operation>&',`GeglOperation*',__CONVERT_REFPTR_TO_P)

_CONVERSION(`const Glib::RefPtr<NodeContext>&',`GeglNodeContext*',__CONVERT_REFPTR_TO_P)

_CONVERSION(`GeglColor*',`Glib::RefPtr<Color>',Glib::wrap($3))
_CONVERSION(`GeglColor*',`Glib::RefPtr<const Color>',Glib::wrap($3))
_CONVERSION(`const Glib::RefPtr<Color>&',`GeglColor*',__CONVERT_REFPTR_TO_P)

_CONVERSION(`GObject*',`Glib::RefPtr<Parameter>',`Glib::wrap($3)')
_CONVERSION(`GObject*',`Glib::RefPtr<const Parameter>',`Glib::wrap($3)')

_CONVERSION(const Gegl::Rectangle&,GeglRectangle*,__CFR2P)
_CONVERSION(const Rectangle&,GeglRectangle*,__CFR2P)
_CONVERSION(`const Rectangle&',`const GeglRectangle*',__CFR2P)
_CONVERSION(`GeglRectangle*',`Rectangle',`Glib::wrap($3)')
_CONVERSION(`const GeglRectangle*',`Rectangle',`Glib::wrap($3)')
#_CONVERSION(`const GeglRectangle*',`const Rectangle &',`Glib::wrap($3)')
_CONVERSION(`GeglRectangle',`Rectangle',`Rectangle($3)')

_CONVERSION(`GSList*',`Glib::SListHandle< Glib::RefPtr<Node> >',`$2($3, Glib::OWNERSHIP_SHALLOW)')


_CONVERSION(`const GObject*',`Glib::RefPtr<const Glib::Object>',`Glib::wrap(const_cast<GObject*>($3))')
_CONVERSION(`const Glib::RefPtr<Glib::Object>&', `const GObject*', `($3)->gobj()')


_CONVERSION(`const Value&', `GValue*', `const_cast<GValue*>(($3).gobj())')
_CONVERSION(`Glib::Value&', `GValue*', `($3).gobj()')
_CONVERSION(`const Glib::Value&', `const GValue*', `($3).gobj()')
_CONVERSION(`GValue*', `Value', `$2($3)')
_CONVERSION(`const GValue*', `Value', `$2($3)')

_CONVERSION(`const int*','const-gint*',`$3')

_CONVERSION(`long&',`glong*',`&($3)')

_CONVERSION(`Glib::ustring', `const gchar*', `($3).c_str()')

# Lists
_CONVERSION(`const Glib::ListHandle<Value>&',`GList*',`$3.data()')
_CONVERSION(`GList*',`const Glib::ListHandle<Value>',__FL2H_SHALLOW)
_CONVERSION(`GSList*',`Glib::SListHandle<Parameter>',__FL2H_SHALLOW)
_CONVERSION(`const Glib::SListHandle<Parameter>&',`GSList*',`$3.data()')


# For signals:

