/* $Id: generate_defs_gegl.cc 1017 2007-02-14 23:03:00Z arminb $ */

/* generate_defs_gegl.cc
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream>
#include "glibmm_generate_extra_defs/generate_extra_defs.h"
#include <gegl.h>
#include <gegl-tile-backend.h>
#include <gegl-tile-source.h>
#undef HAVE_CONFIG_H
#include <gegl-plugin.h>

int main (int argc, char *argv[])
{
    gegl_init(&argc, &argv);

    std::cout << get_defs(GEGL_TYPE_NODE)
	      << get_defs(GEGL_TYPE_RECTANGLE)
	      << get_defs(GEGL_TYPE_COLOR)
	      << get_defs(GEGL_TYPE_PARAM_COLOR)
	      << get_defs(GEGL_TYPE_BUFFER)
	      << get_defs(GEGL_TYPE_CURVE)
	      << get_defs(GEGL_TYPE_PARAM_CURVE)
	      << get_defs(GEGL_TYPE_PROCESSOR)
	      << get_defs(GEGL_TYPE_PATH)
	      << get_defs(GEGL_TYPE_PARAM_PATH)
	      << get_defs(GEGL_TYPE_PARAM_STRING)
	      << get_defs(GEGL_TYPE_PARAM_FILE_PATH)
	      << get_defs(GEGL_TYPE_PARAM_MULTILINE)
	      << get_defs(GEGL_TYPE_PARAM_ENUM)
	      << get_defs(GEGL_TYPE_TILE_BACKEND)
	      << get_defs(GEGL_TYPE_TILE_SOURCE)
	      << get_defs(GEGL_TYPE_OPERATION)
	      << get_defs(GEGL_TYPE_OPERATION_SOURCE)
	      << get_defs(GEGL_TYPE_OPERATION_SINK)
	      << get_defs(GEGL_TYPE_OPERATION_FILTER)
	      << get_defs(GEGL_TYPE_OPERATION_COMPOSER)
	      << get_defs(GEGL_TYPE_OPERATION_COMPOSER3)
	      << get_defs(GEGL_TYPE_OPERATION_POINT_COMPOSER)
	      << get_defs(GEGL_TYPE_OPERATION_POINT_COMPOSER3)
	      << get_defs(GEGL_TYPE_OPERATION_POINT_FILTER)
	      << get_defs(GEGL_TYPE_OPERATION_POINT_RENDER)
	      << get_defs(GEGL_TYPE_OPERATION_AREA_FILTER)
	      << get_defs(GEGL_TYPE_OPERATION_META)
              << get_defs(GEGL_TYPE_OPERATION_TEMPORAL)
		;
	gegl_exit();
    return 0;
}
